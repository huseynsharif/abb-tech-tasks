package com.abbtech.lesson1homework

fun main(){

    var table = workTable("Black", 140);
    table.printInfo()



}

open class Table(
    var color:String ?= null,
    var height:Int = 0
){
    open fun printInfo(){
        println("$color, $height")
    }
}


class workTable(color:String, height:Int) : Table(color, height){
    override fun printInfo(){
        super.printInfo();
        println("Details here...")
    }
}
