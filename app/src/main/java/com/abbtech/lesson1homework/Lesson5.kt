package com.abbtech.lesson1homework

fun main() {
//
//    val mapof = mapOf(
//        1 to "Mercury",
//        2 to "Venus",
//        3 to "Earth",
//        4 to "Mars",
//        5 to "Jupiter",
//        6 to "Saturn",
//        7 to "Uranus",
//        8 to "Neptune"
//        )
//
//    mapof.forEach{
//        println(it)
//    }

    // list -> classes -> studentNo, StudentName


//    var students = listOf<Student2>(Student2(1, "Huseyn"), Student2(2), Student2(3, "Mehemmed"), Student2())
//
//    var newStudents = students.filter {
//        it.studentName != null && it.studentNo!=null
//    }
//
//    println(newStudents)

//    try {
//        getName(null)
//    }
//    catch (e:Exception){
//        println(e.message)
//        println(e.localizedMessage)
//        e.printStackTrace()
//    }

    try {
        orbit(160)
    }
    catch (e:Exception){
        e.printStackTrace()
    }


}

fun orbit(d:Int){

    when{
        d>152 -> throw outOfOrbitException()
        d<148 -> throw lessThanUsualException()
        else -> println(d)
    }


}


class outOfOrbitException():Exception("We are getting out of orbit.")
class lessThanUsualException():Exception("We are getting hot.")

//
//fun getName(name:String?):String{
//
////    if (name==null){
////        throw Exception("Cannot be null!")
////    }
//
//    return name!!
//}
//
//
//data class Student2(
//    var studentNo:Int ? = null,
//    var studentName:String ? = null
//)