package com.abbtech.lesson1homework


fun main(){

    var circle = Circle(5)
    circle.also {
        it.radius = 6
    }

    

    var rectangle = Rectangle(4, 10)
    rectangle.apply {
        this.height = 3
        this.weight = 5
    }

    var triangle = Triangle(3, 4, 5)
    with(triangle){
        a = 6
        b = 8
        c = 10
    }

    var a = circle isCircle triangle


}

open class Shape(var typeOfShape:String){
    open fun type():String{
        return this.typeOfShape;
    }

    open fun calculateArea():Double{
        // TODO: default ne olsun???
        return 0.0;
    }
}

class Circle(var radius:Int):Shape("circle") {
    override fun calculateArea(): Double {
        var s = Math.PI * radius * 2
        return s
    }

    infix fun isCircle(shape: Shape):Boolean{
        //logical codes

        if (shape.typeOfShape == "circle"){
            return true;
        }
        else{
            return false
        }

    }
}

class Rectangle(var weight:Int, var height:Int):Shape("rectangle"){
    override fun calculateArea(): Double {
        return weight.toDouble() * height.toDouble()
    }

    infix fun Math.isRectangle(shape:Shape):Boolean{
        // logical codes
        return true
    }
}

class Triangle(var a:Int,var b:Int,var c:Int):Shape("triangle"){
    override fun calculateArea(): Double {
        var s = 0.0 // dustur
        return s;
    }

    infix fun Math.isTriangle(shape: Shape):Boolean{
        // logical codes
        return true
    }
}

//open class Employee{
//
//    lateinit var name:String;
//    lateinit var surname:String;
//    lateinit var adress:String;
//
//    var salary:Int = 0;
//    var age:Int = 0;
//
//    /**
//     * It can be called by everyone
//     */
//    public fun introduce(){
//        println("I am $name $surname")
//    }
//
//    /**
//     * It can be called by workers
//     */
//    internal fun mySalary(): Int {
//        return this.salary;
//    }
//
//    /**
//     * It is personal information
//     */
//    private fun myAdress(){
//        println("My adress is $adress")
//    }
//
//    /**
//     * It can be called by subclasses
//     */
//    protected fun myAge(){
//        println("My age is $age")
//    }
//
//
//}