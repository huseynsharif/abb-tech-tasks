package com.abbtech.lesson1homework

fun main(){
//    val w1 = "Hi."
//    val w2 = "My"
//    val w3 = "name"
//    val w4 = "is"
//    val w5 = "Huseyn"
//
//    val line1 = w1 + " " +w2 + " " +w3 + " " +w4 + " " +w5 + " ";
//    val line2 = "$w1 $w3 $w3 $w4 $w5";
//    val r = """
//        $line1
//        $line2
//    """.trimIndent()
//    println(r)

//    val value1 = Triple(2, 3, 4);
//    val results = Pair(value1.first>value1.second, value1.second>value1.third)
//    println(results.first)
//    println(results.second)

//    val score = 60
//    if(score>90){
//        println("A")
//    }
//    else if(score>80){
//        println("B")
//    }
//    else if(score>70){
//        println("C")
//    }
//    else if(score>60){
//        println("D")
//    }
//    else if(score>50){
//        println("E")
//    }
//    else{
//        println("FAILED")
//    }

//    for(score in 0..100 step 10){
//        var r = when {
//            score >90 -> "A"
//            score >80 -> "B"
//            score >70 -> "C"
//            score >60 -> "D"
//            score >50 -> "E"
//            else -> "FAILED"
//        }
//        println(r)
//    }


//    var v = 1;
//    while (v<=50){
//        if (v%7==0) println(v)
//        v++
//    }
//

    var huseyn = Student("Huseyn", "Sharifzade", 100)
    if(huseyn.score>70) println("""
        ${huseyn.name}
        ${huseyn.surname}
    """.trimIndent())
}

data class Student(
    var name:String,
    var surname:String,
    var score:Int
)