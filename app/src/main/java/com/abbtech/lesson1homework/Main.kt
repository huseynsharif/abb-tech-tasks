package com.abbtech.lesson1homework

fun main(){
    var calculator = Calculator();
    var result = calculator.div(12, 5);
    println(result);

    var name = task2("Huseyn");
    println(name);
}

/**
 * This class has 4 function. It is calculator.
 * @author Huseyn Sharifzade && ABB tech
 */
class Calculator {

    /**
     * @param 2 number
     * @return sum of 2 number
     */
    fun add(a:Int, b: Int) :Int{
        return a+b;
    }

    /**
     * @param 2 number
     * @return subtract of 2 number
     */
    fun subtract(a:Int, b: Int) :Int{
        return a-b;
    }

    /**
     * @param 2 number
     * @return multiplication of 2 number
     */
    fun multiply(a:Int, b: Int) :Int{
        return a*b;
    }

    /**
     * @param 2 number
     * @return divide of 2 number
     */
    fun div(a:Int, b: Int) :Double{
        return a.toDouble()/b.toDouble();
    }
}

/**
 * @author Huseyn Sharifzade && ABB tech
 * @param name, String
 * @return name
 */
fun task2(name :String) :String{
    // return this name
    return name;
}
