package com.abbtech.lesson1homework

fun main() {
    var bileceri = Depot(50, 100, 40, 1500.4)
    var yasamal = Depot(30, 12, 80, 900.5)

    var targovu = bileceri + yasamal

    println(targovu.pencils)
    println(targovu.books)
    println(targovu.maps)
    println(targovu.moneyInside)

    var s = div(5, 4)
    println(s)
}

var mul: (Int, Int) -> Int = { a, b->
    a*b
}
var div: (Int, Int) -> Double = { a, b->
    a.toDouble()/b.toDouble()
}
var minus: (Int, Int) -> Int = { a, b->
    a-b
}
var qaliq: (Int, Int) -> Int = { a, b->
    a%b
}

operator fun Depot.plus(second:Depot):Depot{
    return Depot(
        this.pencils + second.pencils,
        this.books + second.books,
        this.maps + second.maps,
        this.moneyInside + second.moneyInside
    )
}

class Depot(
    var pencils:Int,
    var books:Int,
    var maps:Int,
    var moneyInside:Double,
)