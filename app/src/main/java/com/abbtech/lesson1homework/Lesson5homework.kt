package com.abbtech.lesson1homework

fun main() {

    task1("aa,bb,cc,dd,ee,ff")

//    task2()
//    task3()

    var map = TripleMap<Int, String, Boolean>()
    map.add(1 addTo "Huseyn" addTo true)
    map.add(2 addTo "Shamil" addTo false)
    println(map.size)

    var sds = 5
    map.remove(0)

    var map2  = tripleMapOf<Int, String, Boolean>(
        3 addTo "DS" addTo false,
        4 addTo "DASD" addTo true
    )
    println(map2.size)

}


public fun <K, V, X> tripleMapOf(vararg pairs: TripleMap.TripleEntry<K, V, X>): TripleMap<K, V, X> = buildTripleMap(*pairs)

fun <K, V, X> buildTripleMap(vararg pairs: TripleMap.TripleEntry<K, V, X>): TripleMap<K, V, X> {
    var tripleMap = TripleMap<K, V, X>()
    pairs.forEach {
        tripleMap.add(it)
    }
    return tripleMap
}


infix fun <T, R> T.addTo(element: R): Pair<T, R> = Pair(this, element)

infix fun <T, R, K> Pair<T, R>.addTo(element: K): TripleMap.TripleEntry<T, R, K>{
    return TripleMap.TripleEntry(this.first, this.second, element)
}

class TripleMap<T, R, K>() {
    private val list = mutableListOf<TripleEntry<T, R, K>>()
    fun add(tripleEntry: TripleEntry<T, R, K>){
        list.add(tripleEntry)
    }

    fun remove(index: Int){
        list.removeAt(index)
    }

    val size
        get() = list.size

    fun removeByFirstKey(key:T){
        list.removeIf {
            it.first == key
        }
    }

    fun removeBySecondKey(key:R){
        list.removeIf {
            it.second == key
        }
    }

    class TripleEntry<T, R, K>(
        var first: T,
        var second: R,
        var triple: K
    )
}


fun task3(){
    var numberList = listOf<Int>(1,2,3,4,2,3,3,-7,-5, 4,6,5)
    try {
        checkDuplicates(numberList)
        checkNegatives(numberList)
        var sum = numberList.toSet().sum()
        println(sum)
    }
    catch (e:DuplicateNumberException){
        println("Error: ${e.message}")
    }
    catch (e:NegativeNumberException){
        println("Error: ${e.message}")
    }
}

fun checkDuplicates(numbers:List<Int>){
    var numberSet = mutableSetOf<Int>()
    for(n in numbers){
        if (!numberSet.add(n)){
            throw DuplicateNumberException(n)
        }
    }
}

fun checkNegatives(numbers: List<Int>){
    for(n in numbers){
        if (n<0){
            throw NegativeNumberException(n)
        }
    }
}

class DuplicateNumberException(var d:Int):Exception("There is a duplicate number: $d")

class NegativeNumberException(var n:Int):Exception("There is a negative number: $n")

fun task2(){
    var employees = listOf<Employee>(
        Employee("Huseyn", "6f8lshd", true),
        Employee("Murad", "4jfd8s", false),
        Employee("Shamil", "5dshs7", true),
        Employee(isWorking = true),
        Employee("Tahir", isWorking = true),
        Employee(finNo = "7dxsod0", isWorking =  true),
        Employee("Fariz", "7xsajd9", false)
    )

    employees = employees.filter {
        it.isWorking
    }

    for (e in employees){

        if (e.name.isNullOrBlank()){
            println("Please, enter the name of the employee: ")
            var input = readln()
            while (input.isNullOrBlank()){
                println("Please, enter a valid name: ")
                input = readln()
            }
            e.name = input
        }

        if (e.finNo.isNullOrBlank()){
            println("Please, enter the finNo of the employee: ")
            var input = readln()
            while (input.isNullOrBlank()){
                println("Please, enter a valid finNo: ")
                input = readln()
            }
            e.finNo = input
        }

    }

    println("Cleaned up employees: ")
    employees.forEach{
        println(it)
    }
}

data class Employee(
    var name:String?=null,
    var finNo:String?=null,
    var isWorking:Boolean
)

fun task1(text:String){

    var items = text.split(",")

    var itemsMap = items.mapIndexed { index, item -> (index + 1) to item }.toMap()

    var output = itemsMap.entries.joinToString (", "){ "${it.key} to ${it.value}" }
    println(output)

}