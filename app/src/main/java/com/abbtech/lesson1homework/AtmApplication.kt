package com.abbtech.lesson1homework

import java.lang.NumberFormatException
import kotlin.system.exitProcess

var cards = listOf<Card>(
    Card("1111 1111 1111 1111", 124.5, "1111", CurrencyType.MANAT),
    Card("2222 2222 2222 2222", 100.4, "2222", CurrencyType.DOLLAR),
    Card("3333 3333 3333 3333", 57.8, "3333", CurrencyType.EURO),
    Card("4444 4444 4444 4444", 150.0, "4444", CurrencyType.MANAT),
)

fun main() {
    println("Please, enter a card number: ")
    var cardNumber = readln().trim()
    var cardIndex = cards.containsWithCardNumber(cardNumber)
    while (cardIndex==-1){
        println("Please, enter a valid card number: ")
        cardNumber = readln().trim()
        cardIndex = cards.containsWithCardNumber(cardNumber)
    }

    println("Enter pin:")
    var pinCodeInput = readln().trim()
    var chances = 3
    while (cards[cardIndex].pinCode != pinCodeInput){
        if (chances==1){
            println("Forbidden action!")
            break
        }
        println("Incorrect password! You have ${--chances} left.")
        println("Enter pin:")
        pinCodeInput = readln().trim()
    }

    while (true){
        printOptions()
        var action = readln().toInt()

        try {
            when(action){
                0 -> exitProcess(0)
                1 -> withDraw(cardIndex)
                2 -> checkBalance(cardIndex)
                3 -> increaseBalance(cardIndex)
                4 -> cashByCode(cardIndex)
            }
        }catch (e:ZeroBalanceException){
            println(e.message)
        }
        catch (e:OutOfBalanceException){
            println(e.message)
        }
        catch (e:IllegalInputException){
            println(e.message)
        }
        catch (e:NumberFormatException){
            println("Your input is not correct.")
        }
        catch (e:InvalidCodeException){
            println(e.message)
        }
    }
}

fun cashByCode(cardIndex: Int) {

    var code = randomCodeGenerator()
    var message = "Your code: $code"
    messageToPhone(message) // it is simulation for sending message to phone
    var codeInput = readln().toInt()
    if (code != codeInput){
        throw InvalidCodeException()
    }

    withDraw(cardIndex)
}

fun messageToPhone(message: String) {
    println(message)
}

fun randomCodeGenerator():Int{
    val chars = 0..9
    return (1..4).map { chars.random() }.joinToString("").toInt()
}

fun increaseBalance(cardIndex: Int) {
    var money = inputMoney()
    cards[cardIndex].balance+=money
    println("Successfully increased. Your current balance is: ${cards[cardIndex].balance}${cards[cardIndex].currency.icon}")
}

fun inputMoney():Int{
    println("Enter the money you want:")
    var money = readln().toInt() // Because our ATM cannot use QƏPİK
    if (money<=0){
        throw IllegalInputException()
    }
    return money
}

fun withDraw(cardIndex: Int) {
    if (cards[cardIndex].balance<=0){
        throw ZeroBalanceException()
    }

    var money = inputMoney()

    if (money.toDouble() > cards[cardIndex].balance) {
        throw OutOfBalanceException()
    }

    cards[cardIndex].balance -= money;
    println("Here your money: $money. Your current balance is ${cards[cardIndex].balance}")

}

fun checkBalance(cardIndex: Int) {
    println("Your balance is ${cards[cardIndex].balance} ${cards[cardIndex].currency.icon}")
}


fun printOptions(){
    println("Select action: ")
    println("1) With draw ")
    println("2) Check Balance ")
    println("3) Increase Balance ")
    println("4) Cash by code ")
    println("0) Exit ")

}

class ZeroBalanceException():Exception("Your balance is zero.")
class OutOfBalanceException():Exception("Out of Balance.")
class IllegalInputException():Exception("Input is not correct.")
class InvalidCodeException():Exception("Code is not valid.")

/**
 *   It is a custom contains function for List<Card>
 *   @return Index if exists, else -1
 */
fun List<Card>.containsWithCardNumber(cardNumber:String):Int{
    var i=0;
    while (i < cards.size){

        if(cards[i].cardNumber == cardNumber){
            return i
        }
        i++;
    }
    return -1
}

data class Card(
    val cardNumber:String,
    var balance:Double,
    var pinCode:String,
    val currency:CurrencyType
)

enum class CurrencyType(val icon: String) {
    MANAT("₼"),
    DOLLAR("$"),
    EURO("Є")
}