package com.abbtech.lesson1homework


fun main(){
    var customers = arrayListOf<Customer>()

    while (true){

        var line =  readln()

        if(line.contains("stop")){
            break
        }

        customers.add(Customer(line.split(" ")[0], line.split(" ")[1], line.split(" ")[2].toInt()))
    }

    for (c in customers){
        println(c)
    }
}


data class Customer(
    var name:String,
    var surname:String,
    var balance:Int
)

fun doTask(day: Day): Tasks {
    return if(day.name == "FRIDAY"){
        Tasks.goMasjid();
    }
    else if(day.name=="SUNDAY"){
        Tasks.restDay()
    }
    else {
        Tasks.goGym()
    }

}

enum class Day{
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

sealed class Tasks{
    class goGym():Tasks()
    class goMasjid():Tasks()
    class restDay():Tasks()
}

interface CosmicObject{
    fun name()
}

class Planet:CosmicObject{
    override fun name() {
        println("I am planet.");
    }
}

class Star:CosmicObject{
    override fun name() {
        println("I am star.")
    }
}


